"""

The venv handler, as the name suggests, handles the handling of python's
virtual environments if and only if a requirements.txt file is present.

My venv handler of choice would be python3-virtualenv. This needs to be
installed, otherwise the venv handler cannot function.

When imported to other programs, this script checks if a requirements.txt file
is present and if it checks the existence of a virtual environment named "venv"
and if the required packages from the requirements.txt are installed within.

Overview:
    1. No requirements.txt --> Do nothing and be done ✓
    2. "venv" not existing --> Create Virtual Environment and install requirements within.
    3. "venv" is  existing --> Check requirements, install or upgrade packages when needed.
    4. "venv" is  existing --> All requirements satisfied --> be done ✓

"""


################################################################################

import os
requirements_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'requirements.txt')

# Are there any requirements that require a virtual environment?
if (requirements_file):
    # --------------------------------------------------------------------------
    # only now necessary libraries will be imported
    import sys
    import subprocess
    import importlib.metadata
    written_to_stdout = False


    # ---------------------------------------------------------------------------
    # Check if python3-virtualenv is installed
    try:
        subprocess.check_output(['dpkg', '-s', 'python3-virtualenv'])

    except subprocess.CalledProcessError:
        sys.exit('python3-virtualenv needs to be installed to run this script.')


    # --------------------------------------------------------------------------
    # Check if the virtual environment directory "venv" exists, else create it
    venv_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'venv')

    if not os.path.isdir(venv_path):
        subprocess.check_call(['virtualenv', venv_path])
        written_to_stdout = True


    # --------------------------------------------------------------------------
    # Activate the virtual environment
    activate_this = os.path.join(venv_path, 'bin/activate_this.py')
    with open(activate_this) as file:
        exec(file.read(), dict(__file__=activate_this))


    # --------------------------------------------------------------------------
    # Read in requirements from the requirements.txt file
    with open(requirements_file) as f:
        requirements = [line.strip() for line in f.readlines()]


    # --------------------------------------------------------------------------
    # Check if each requirement is installed, if not, install it into the venv
    for package_name in requirements:
        try:
            importlib.metadata.distribution(package_name)
        except ModuleNotFoundError:
            subprocess.check_call([os.path.join(venv_path, 'bin', 'pip'), 'install', package_name])
            written_to_stdout = True


    # --------------------------------------------------------------------------
    # Print a single empty line to separate the main program from the venv-handler
    if written_to_stdout:
        print()

################################################################################

