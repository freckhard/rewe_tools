# rewe_tools
Sort electronic receipts ("eBons") from REWE food market and archive them. Then sum over all receipts to get the total for the current year.

# rewe_mails
Get eBons PDF straight from your IMAP mailbox! If an eBon mail is unread, the pdf attachment will be downloaded and the message will be marked as read, and if desired moved to an existing IMAP folder of your choice for archiving.

# Newest features
I am introducing a _venv_handler_ which - as the name suggests - handles the handling of python's virtual environments. If and only if a requirements.txt file is present, a virtual environment is being created by using the `python3-virtualenv` system package. If created or already present, the requirements will be checked and installed if necessary.

What's that about? You can just run this script without needing to pip install the requirements and by the nature of virtual environments your system packages won't be touched. Easy of mind & ease of use!

# Additional Information
Please adjust the year and your paths according to your own directory structure, as I deliberately hardcoded them to keep it simple.
For rewe_tools, you can optionally set a year sysarg, for which the eBons should be summed (again), like
```
$ python rewe_tools.py 2021
```

## Future-Nice-To-Haves:
Have the eBon figure out its own date and have it sorted accordingly, or not have it sorted if a specified year was given before.

## Upcoming
A major rework of `rewe_tools` is planned, as my python improved dramatically over the past 2 years.
Some things that I would like to implement or address are:
- much cleaner code structure following pylint / pep8 rules and guidelines
- making use of eBon objects with automatically sorting by its year
- clean up hardcoded-paths and make them configurable via yaml
- somehow ensure os-independence or the venv_handler

Stay tuned!