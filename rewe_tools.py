import venv_handler
import os
import sys
import glob
import hashlib
import re as regex
import fitz as pypdf
from datetime import date
from send2trash import send2trash

rewrite_last_line = "\033[A" + 40 * " " + "\033[A"

def get_or_set_default_year():
    if len(sys.argv) > 2:
        print("Bitte nur das Jahr in 20YY angeben.")
        exit()
    if len(sys.argv) == 2:
        if not regex.search("^20\\d{2}$", sys.argv[1]):
            print("Bitte nur das Jahr in 20YY angeben.")
            exit()
        year = sys.argv[1]
    else:
        year = str(date.today().year)
    
    print("REWE Tools für", year, "gestartet.")
    return year


def check_for_path_and_file():
    if not os.path.isdir(path):
        if not input(f"Pfad {path} existiert noch nicht, soll dieser erstellt werden? (j/n): ") == "j":
            exit("Ordner wird nicht erstellt. Abbruch des Programms.")
        os.makedirs(path)

    if not os.path.isfile(path + sum_file):
        get_total_off_all_ebons()


def save_and_print_total_sum():
    with open(path + sum_file, 'w') as file:
        filesum = round(summe, 2)
        filesum = format(summe, '.2f')
        file.write(filesum)

    print("\nDie Gesamtsumme in", year, "beträgt", filesum, "€")


def get_file_hash(file):
    with open(file, "rb") as f:
        sha256_hash = hashlib.sha256()
        for byte_block in iter(lambda: f.read(4096), b""):
            sha256_hash.update(byte_block)

    return sha256_hash.hexdigest()


def get_hash_list():
    hash_list = []
    files = glob.glob(path + "*.pdf")
    for file in files:
        hash_list.append(get_file_hash(file))

    return hash_list


def find_and_process_ebons():
    list_of_pdfs = glob.glob(ablage + "*.pdf")

    eBon_hashs = get_hash_list()

    for pdf_object in list_of_pdfs:

        if get_file_hash(pdf_object) in eBon_hashs:
            print("\n" + pdf_object, "ist bereits identisch vorhanden.")
            if input("PDF löschen? ") == 'y':
                send2trash(pdf_object)
                print(rewrite_last_line)
                print("PDF gelöscht!")
            else:
                print(rewrite_last_line)
                print("PDF wurde nicht gelöscht.")
            continue

        pdf = pypdf.open(pdf_object)

        pdf_text = ""

        for page_num in range(pdf.page_count):
            pdf_text += pdf.load_page(page_num).get_text("text")

        if regex.search("REWE.*PAYBACK", pdf_text):
            lines = pdf_text.split("\n")

            for line in lines:
                if regex.search("SUMME", line):
                    line = line.replace(",", ".")
                    amount = float(regex.search("\\d.*", line).group())

                if regex.search("Datum", line):
                    date = regex.search("(\\d{2})\\.(\\d{2})\\.(\\d{4})", line)
                    dd, mm, yyyy = date.group(1), date.group(2), date.group(3)

            strings = ["REWE", yyyy, mm, dd, "eBon"]
            filename = "_".join(strings)

        else:
            continue # skip to next pdf if not a REWE eBon

        if os.path.isfile(path + filename + ".pdf"):
            bon_counter = 2
            while os.path.isfile(path + filename + "_" + str(bon_counter) + ".pdf"):
                bon_counter += 1

            filename += "_" + str(bon_counter)

        os.rename(pdf_object, path + filename + ".pdf")
        print("Moving", filename + ".pdf ... (" + str(format(amount, '.2f')) + "€)")


def get_total_off_all_ebons():
    global summe
    summe = 0.0

    # get a list of all ebon pdfs in REWE dir
    ebons_list = glob.glob(path + "*.pdf")

    # iterate over each individual eBon.pdf
    for ebon_pdf in ebons_list:

        # use fitz library to open current pdf as ebon pdf object
        ebon = pypdf.open(ebon_pdf)

        # loop through pages
        for page_num in range(ebon.page_count):

            # get Text from current page
            page_content = ebon.load_page(page_num).get_text("text")
            lines = page_content.split("\n")

            for line in lines:
                if regex.search("SUMME", line):
                    text = line.replace(",", ".")
                    amounts = regex.search("\\d.*", text).group()
                    summe += float(amounts)


""" begin of main programm """
year = get_or_set_default_year()
ablage = "/home/eckhard/Daten/Ablage/"
path = "/home/eckhard/Daten/Archiv/Familie/" + year + "/REWE/"
sum_file = "REWE_" + year + "_Summe.txt"

check_for_path_and_file()
find_and_process_ebons()
get_total_off_all_ebons()
save_and_print_total_sum()


""" future-nice-to-haves """
# get date-year from eBon and sort accordingly
# make use of os.joinpath to make it linux independent
# remove redundancy into extra functions
# make functions more lightweight / smaller overall
# use classes --> eBon objects!
