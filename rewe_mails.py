""" This scriptlet will scan the IMAP Inbox for any unread desired-E-Mails,
    downloads their PDF attachment to the save_dir whilst also taking care of duplicates.
    It will mark the corresponding mails as read and moves the processed mails to
    the given archive Folder for the purpose of archiving. """

import email
import imaplib
import os
import yaml


def set_mailbox_credentials(login_yaml):
    """ Loads mailserver credentials from yaml """

    credentials = read_credentials_yaml(login_yaml)

    LOGIN['IMAP_URL'] = credentials['email']['imap_host']
    LOGIN['USERNAME'] = credentials['email']['username']
    LOGIN['PASSWORD'] = credentials['email']['password']


def read_credentials_yaml(login_yaml):
    """ Loads credentials yaml from filesystem """

    path = os.path.realpath(os.path.join(
        os.getcwd(), os.path.dirname(__file__)))
    path = path + '/' + login_yaml

    with open(path, 'r', encoding='utf8') as stream:
        try:
            credentials = yaml.load(stream, Loader=yaml.FullLoader)
        except yaml.YAMLError as exc:
            print(exc)

    return credentials


def set_filename(filename):
    """ Sets the download filename. If already
        present, it increments the filename."""

    filename = filename[:filename.find('.pdf')]

    if os.path.isfile(SAVE_DIR + filename + '.pdf'):
        counter = 2

        while os.path.isfile(SAVE_DIR + filename + '_' + str(counter) + '.pdf'):
            counter += 1

        filename += '_' + str(counter)

    return filename + '.pdf'


def download_mail_from_and_move_to(reciepient, archive=''):
    """ This function handles the login, processes all wanted mails,
        downloads their attachments and moves the read mail to the
        desired archive folder. Then it closes the connection. """

    connection = imaplib.IMAP4_SSL(LOGIN['IMAP_URL'])
    connection.login(LOGIN['USERNAME'], LOGIN['PASSWORD'])
    connection.select('INBOX', readonly=False)

    numer_of_attachments_downloaded = 0

    reciepient = 'FROM ' + reciepient

    _status, messages = connection.uid('search', None, reciepient, 'Unseen')

    inbox_item_list = messages[0].split()

    for inbox_item in inbox_item_list:

        attachment_downloaded = False

        _status, mail_data = connection.uid('fetch', inbox_item, '(RFC822)')

        email_content = mail_data[0][1].decode('utf-8')
        email_message = email.message_from_string(email_content)

        print("Verarbeitung von:", email_message['Subject'])

        for part in email_message.walk():

            if part.get_content_maintype == 'multipart':
                continue

            if filename := part.get_filename():
                filename = set_filename(filename)

                print("Speichere Anhang:", os.path.join(
                    SAVE_DIR, filename), '\n')

                with open(SAVE_DIR + filename, 'wb') as attachment:
                    attachment.write(part.get_payload(decode=True))

                attachment_downloaded = True
                numer_of_attachments_downloaded += 1

        if archive and attachment_downloaded:
            connection.uid('MOVE', inbox_item, archive)


    connection.close()

    print("#", numer_of_attachments_downloaded, "attachments downloaded", reciepient, "\n")
    print((100 * '-') + "\n")

################################################################################


SAVE_DIR = '/path/to/download/dir/'

LOGIN = {'IMAP_URL': '', 'USERNAME': '', 'PASSWORD': ''}

set_mailbox_credentials('credentials.yaml')
download_mail_from_and_move_to('ebon@mailing.rewe.de', 'REWE')
download_mail_from_and_move_to('invoice@example.com', 'Rechnungen')
download_mail_from_and_move_to('info@example.com')
